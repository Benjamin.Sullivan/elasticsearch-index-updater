#!/bin/bash

source util/_common.sh

trap "echo && return" SIGINT

log "[---Run start---]"

GET_INDEX_INFO="Get remote index info"
GET_INDEX_INFO_SCRIPT() {
    log "<- [$GET_INDEX_INFO] ->"
    . scripts/get_index_info.sh
}

GET_INDEX_STATS="Get remote index stats"
GET_INDEX_STATS_SCRIPT() {
    log "<- [$GET_INDEX_STATS] ->"
    . scripts/get_index_stats.sh
}

CHECK_INDEX="Scan remote index for deprecations"
CHECK_INDEX_SCRIPT() {
    log "<- [$CHECK_INDEX] ->"
    . scripts/identify_deprecations.sh
}

CHECK_INDICES="Scan all remote indices for deprecations"
CHECK_INDICES_SCRIPT() {
    log "<- [$CHECK_INDICES] ->"
    . scripts/scan_indices_for_deprecations.sh
}

CHECK_LOCAL_JSON_FILE="Check local JSON file for deprecations"
CHECK_LOCAL_JSON_FILE_SCRIPT() {
    log "<- [$CHECK_LOCAL_JSON_FILE] ->"
    . scripts/check_local_json.sh
}

CURE_LOCAL_JSON_FILE="Attempt to fix deprecations in local JSON file"
CURE_LOCAL_JSON_FILE_SCRIPT() {
    log "<- [$CURE_LOCAL_JSON_FILE] ->"
    . scripts/fix_json.sh
}

CREATE_INDEX="Create index"
CREATE_INDEX_SCRIPT() {
    log "<- [$CREATE_INDEX] ->"
    . scripts/create_index.sh
}

REINDEX="Reindex"
REINDEX_SCRIPT() {
    log "<- [$REINDEX] ->"
    log "Select a source index and a destination index"
    . scripts/reindex.sh
}

MOVE_ALIASES="Move aliases"
MOVE_ALIASES_SCRIPT() {
    log "<- [$MOVE_ALIASES] ->"
    . scripts/move_aliases.sh
}

FIX_INDEX="Attempt full remote index update"
FIX_INDEX_SCRIPT() {
    log "<- [$FIX_INDEX] ->"
    . scripts/fix_index.sh
}

CHECK_CLUSTERS_FOR_INDEX="Check clusters for index presence"
CHECK_CLUSTERS_FOR_INDEX_SCRIPT() {
    log "<- [$CHECK_CLUSTERS_FOR_INDEX] ->"
    . scripts/check_index_presence.sh
}

DIFF_REMOTE_INDICES="Diff two remote indices"
DIFF_REMOTE_INDICES_SCRIPT() {
    log "<- [$DIFF_REMOTE_INDICES] ->"
    . scripts/diff_indices.sh
}

EXIT="Quit"
EXIT_SCRIPT() {
    log "[---Run end---]" >> $LOG_FILE
    log "Dropping to docker shell"
    exec bash
}

get_selection() {
    options=(
        "$EXIT"
        # "$FIX_INDEX"
        "$DIFF_REMOTE_INDICES"
        "$CHECK_CLUSTERS_FOR_INDEX"
        "$CHECK_INDEX"
        "$MOVE_ALIASES"
        "$REINDEX"
        "$CREATE_INDEX"
        "$CHECK_LOCAL_JSON_FILE"
        "$CURE_LOCAL_JSON_FILE"
        # "$CHECK_INDICES"
        # "$GET_INDEX_STATS"
        "$GET_INDEX_INFO"
    )

    local len=${#options[@]}

    selected_option=$(for (( i=$len; i>0; i-- )); do
        if [ $i -eq $len ]; then
            printf '%s\n' "q) ${options[$len-$i]}"
        else
            printf '%d. %s\n' "$i" "${options[$len-$i]}"
        fi
    done | fzf --header "Select an Option:" --cycle)

    echo "$selected_option" | cut -d ' ' -f2-
}

execute_option() {
    case $1 in
        "$CHECK_INDEX")
            CHECK_INDEX_SCRIPT
            ;;
        "$CHECK_INDICES")
            CHECK_INDICES_SCRIPT
            ;;
        "$CHECK_LOCAL_JSON_FILE")
            CHECK_LOCAL_JSON_FILE_SCRIPT
            ;;
        "$CURE_LOCAL_JSON_FILE")
            CURE_LOCAL_JSON_FILE_SCRIPT
            ;;
        "$FIX_INDEX")
            FIX_INDEX_SCRIPT
            ;;
        "$GET_INDEX_INFO")
            GET_INDEX_INFO_SCRIPT
            ;;
        "$GET_INDEX_STATS")
            GET_INDEX_STATS_SCRIPT
            ;;
        "$CREATE_INDEX")
            CREATE_INDEX_SCRIPT
            ;;
        "$REINDEX")
            REINDEX_SCRIPT
            ;;
        "$MOVE_ALIASES")
            MOVE_ALIASES_SCRIPT
            ;;
        "$CHECK_CLUSTERS_FOR_INDEX")
            CHECK_CLUSTERS_FOR_INDEX_SCRIPT
            ;;
        "$DIFF_REMOTE_INDICES")
            DIFF_REMOTE_INDICES_SCRIPT
            ;;
        "$EXIT")
            EXIT_SCRIPT
            ;;
        *)
            log "Invalid option: $1"
            ;;
    esac

    read -p "- Press any key to return to the menu -"
}

while true; do
    . util/_reset.sh
    choice=$(get_selection)
    if [ -n "$choice" ]; then
        execute_option "$choice"
    else
        EXIT_SCRIPT
    fi
done
