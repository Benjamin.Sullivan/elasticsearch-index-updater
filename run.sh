#!/bin/sh

git pull
echo "Building Docker image"
docker build -q -t dcmcap2-updater .
docker run --rm -it -v $(pwd)/output:/dcmcap2/output dcmcap2-updater
