# Elasticsearch Index Migration Tool
This script bundles common functions needed to update and migrate our indices to the new 5.6 environment. It will assist fixing deprecations on our existing clusters to prepare an index for the move to 5.6. Once the index is ready, the DCM script to move it to the new cluster is [here](https://gitlab.com/angi1/engineering/osms-java/-/blob/master/copy-indexes-to-es56.sh?ref_type=heads).

## Getting started

0) Make sure you are on the VPN and have access to the Engineering VPN - you might need to submit a ticket for access
1) Make sure you are running Docker and have Docker CLI
2) Clone repo & cd in
3) `./run.sh`
4) Git will pull the latest branch, Docker will build an image, and then start a container that will run the script

### Menu Options
1) Get remote index info - select a cluster and an index to pull the index definition. This will also create json files in the output directory storing the full index definition and the extracted mappings and settings of the index.

2) Attempt to fix deprecations in local JSON file - there are some deprecations that can be fixed with jq, run the mappings-settings file through this and it will adjust the json of the file to fix what it can.

3) Check local JSON file for deprecations - select a mappings-settings.json file created by option 1) to run through the deprecation scanner. You can adjust the file on your local machine and run it again to check.

4) Create index - select a local mappings-settings.json file to create a new index on the target cluster.

5) Reindex - select source and destination clusters to reindex documents from A to B.

6) Move aliases - select source and destination clusters to move the aliases from A to B.

7) Scan remote index for deprecations - select a cluster and an index, and the script will check the index definition for known deprecations. (This may not be 100% comprehensive but will catch some)

8) Check clusters for index presence - pick an index and check each cluster for that index name or it's aliases, to see where it's been created

9) Diff two remote indices - compare the definitions of two indices on the same cluster

### Happy Path
For a target cluster (example: ES2.4 Dev = main-elasticsearch.dev.angi.clud)
1) Get remote index info to pull the mappings-settings for an index onto your machine
2) Attempt to fix deprecations in local JSON file - will fix what it can
3) Look through the mappings-settings.json to fix other deprecations by hand
4) Create the new index on your target cluster using the mappings-settings.json, using a different name
5) Reindex the documents from old index to new index
6) Move any aliases from the old index to the new index

At this point the index should be ready for the move to 5.6
