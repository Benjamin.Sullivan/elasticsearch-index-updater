FROM alpine:latest

RUN apk add --no-cache fzf bash jq curl

WORKDIR /dcmcap2
RUN mkdir output
COPY ./*.sh ./
COPY ./util/ ./util/
COPY ./scripts/ ./scripts/

CMD ["./start.sh"]
