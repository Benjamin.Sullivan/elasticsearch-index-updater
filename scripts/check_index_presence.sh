#!/bin/bash

source util/_common.sh

source ./util/_select_env.sh
[ -z "$cluster" ] && log "! No cluster selected" && return
log "Selected cluster: $cluster"

source ./util/_select_index.sh $cluster
[ -z "$index" ] && log "! No index selected" && return
log "Selected index: $index"

source ./util/_index_info.sh $cluster $index
[ -z "$index_information_json_file" ] && log "! No index information available" && return

aliases=($(jq -r '.[] | .aliases | keys | .[]' $index_information_json_file))

options=(
  "$DEV24_CLUSTER"
  "$QA24_CLUSTER"
  "$COMPARK_CLUSTER"
  "$CORNELL_CLUSTER"
  "$DEV_GENERAL56_CLUSTER"
  "$DEV_PERFORMANCE56_CLUSTER"
  "$DEV_STORAGE56_CLUSTER"
  "$STG_GENERAL56_CLUSTER"
  "$STG_PERFORMANCE56_CLUSTER"
  "$STG_STORAGE56_CLUSTER"
  "$PROD_GENERAL56_CLUSTER"
  "$PROD_PERFORMANCE56_CLUSTER"
  "$PROD_STORAGE56_CLUSTER"
)

function check_for_presence() {
    response=$(curl -o /dev/null -s -w "%{http_code}\n" -X HEAD "$1/$2" -i)
    if [[ $response == 2* ]]; then
        return 1
    else
        return 0
    fi
}

for url in "${options[@]}"; do
    check_for_presence $url $index
    index_presence=$?

    if [ $index_presence -eq 1 ]; then
        log "${cluster_name[$url]}: present"
    else
        if [ -n "${aliases[*]}" ]; then
            index_found=0
            for alias in "${aliases[@]}"; do
                check_for_presence $url $alias
                alias_presence=$?
                if [ $alias_presence -eq 1 ]; then
                    log "${cluster_name[$url]}: present through alias: $alias"
                    index_found=1
                    break
                fi
            done
            if [ $index_found -eq 0 ]; then
                log "${cluster_name[$url]}: Not present"
            fi
        else
            log "${cluster_name[$url]}: Not present"
        fi
    fi
done
