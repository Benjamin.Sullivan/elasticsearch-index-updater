#!/bin/bash

source util/_common.sh

trap "echo && return" SIGINT

source ./util/_select_env.sh
[ -z "$cluster" ] && log "! No cluster selected" && return
log "Selected cluster: $cluster"

read -p "enter new index name: " new_index_name

[ -z "$new_index_name" ] && log "! Can't enter blank name" && return
log "New index name: $new_index_name"

CREATE_INDEX_HEADER="
Select an index mappings-settings.json file to create a new index

If not present in the output directory, go back to the
menu and run 'Get remote index info' to create this file.
"

file=$(find . -type f -name '*mappings-settings.json' | fzf --header="$CREATE_INDEX_HEADER"| tr -d '[:space:]')
[ -z "$file" ] && log "! No file selected" && return
log "Selected file: $file"

continue=$(check_continue)

local response=$OUTPUT_DIR/response.temp
status_code=$(curl -XPUT "$cluster/$new_index_name" -d "@$file" -w "%{http_code}" -o $response -s)
log "Request status: $status_code"
log "Response: $(cat $response)"
rm $response


