#!/bin/bash

source util/_common.sh

next_step() {
    echon "--------------------"
    print_time
    echo "NEXT STEP:"
    echo " $1"
    echo "--------------------"
    echo ""
}

intro_message() {
    echon "--------------------"
    echo "This is a script to walk through the DCMCAP2 index migration"
    echon "tl;dr - pull the index info, fix deprecations in the JSON,"
    echo "create a new index, reindex documents to the new index,"
    echo "move aliases from old to new index."
    print_time
    echo "--------------------"
}

has_unfixed() {
    result=$(util/_scan_deprecations.sh "$new_index_mapping_settings_file")

    if [ -n "$result" ]; then
        return 0
    else
        return 1
    fi
}


recap() {
    echon ">-------------------"
    echo " Recap:"
    [[ $has_porter_stem == "true" ]] && echon " porterStem deprecation fixed"
    [[ $has_geohash == "true" ]] && echon " geohash deprecation fixed"
    [[ $has_geohash_precision == "true" ]] && echon " geohash_precision deprecation fixed"
    [[ $has_geohash_prefix == "true" ]] && echon " geohash_prefix deprecation fixed"
    [[ $has_lat_lon == "true" ]] && echon " lat_lon deprecation fixed"
    [[ $has_timestamp == "true" ]] && echon " _timestamp still needs to be fixed"
    [[ $has_ttl == "true" ]] && echon " _ttl still needs to be fixed"
    [[ $has_bad_index_property == "true" ]] && echon " index_properties still needs to be fixed"
    echo ">-------------------"
}

complete_message() {
    echon "--------------------"
    echo "Migration Complete!"
    print_time
    echon "Files are available in Docker shell at $index_dir/:"
    ls -1 $index_dir
    echo "--------------------"
}

# diff_files() {
#     jq --sort-keys . $1 > $OUTPUT_DIR/old.json
#     jq --sort-keys . $2 > $OUTPUT_DIR/new.json
#     diff $OUTPUT_DIR/old.json $OUTPUT_DIR/new.json
#     rm $OUTPUT_DIR/old.json
#     rm $OUTPUT_DIR/new.json
# }

extract_mappings_and_settings() {
    jq '{settings: .[].settings, mappings: .[].mappings}' $index_information_json_file > $index_mapping_settings_file
    jq '{settings: .[].settings, mappings: .[].mappings}' $index_information_json_file > $new_index_mapping_settings_file
}

create_storage() {
    index_dir="$OUTPUT_DIR/${cluster_name}/${index_name}"

    echo " - Storing files in $index_dir/"
    if [ -d "$index_dir" ]; then
        echo "Directory $index_dir exists from previous run. Delete?"
        check_continue
        rm -rf $index_dir
        echo "dir $index_dir deleted"
    fi
    mkdir -p $index_dir

    index_information_json_file=$index_dir/$index_name.json
    index_mapping_settings_file="$index_dir/${index_name}_mappings_settings.json"
    new_index_information_json_file=$index_dir/new_$index_name.json
    new_index_mapping_settings_file="$index_dir/new_${index_name}_mappings_settings.json"
}

update_new_index_file_names() {
    mv "$new_index_information_json_file" "$index_dir/$new_index_name.json"
    mv "$new_index_mapping_settings_file" "$index_dir/mappings_and_settings_$new_index_name.json"
}

check_index_exists() {
    index_exists_status=$(curl -s -o /dev/null -s -w "%{http_code}" "$cluster/$new_index_name?pretty")
    if [ "$index_exists_status" -eq 200 ]; then
        echo "- Index already exists, can't create"
        exit
    elif [ "$index_exists_status" -eq 404 ]; then
        echo "- Index does not exist, can create"
        check_continue
    fi
}

create_new_index() {
    index_created_status=$(curl -s -XPUT -o /dev/null -s -w "%{http_code}" "$cluster/$new_index_name" -d "@$new_index_mapping_settings_file")
    if [ "$index_created_status" -eq 200 ]; then
        info "Index successfully created!"
    else
        alert "Error! Status: $index_created_status"
        exit
    fi
}


#### Script ####

intro_message

source util/_select_env.sh

echon "Fetching indexes for $cluster"
source util/_select_index.sh $cluster
index_name=$index
unset index

echon "Requesting $index_name information"
create_storage
source util/_index_info.sh "$cluster" "$index_name" "$index_information_json_file"
extract_mappings_and_settings

next_step "fix deprecations in index settings and mappings"
util/_fix_deprecations.sh $new_index_mapping_settings_file

echon "Diffing old and new mappings and settings from local JSON:"
diff_files $index_mapping_settings_file $new_index_mapping_settings_file

next_step "check for unfixed deprecations"
if has_unfixed "$new_index_mapping_settings_file"; then
    echon "Cannot proceed to creating the index with unfixed deprecations left in JSON"
    echo "Fix the rest of the deprecations by hand in $new_index_mapping_settings_file"
    echo "Continue by running start.sh, then create index"
    recap
    echon "The JSON files to create the index are available in $index_dir"
    info "Docs: https://www.elastic.co/guide/en/elasticsearch/reference/5.6/breaking_50_mapping_changes.html"
    exit
fi

next_step "create new index"
echo "Current index name: $index_name"
echon "Enter new index name:"
info "New version of Elastic is 5.6.17"
read -p "> " new_index_name
[ -z "$new_index_name" ] && echo "New index name blank" && exit 1

echon "Confirming index name: $new_index_name"
check_continue

echon "Checking if $new_index_name already exists"
check_index_exists

echon "Creating new index: $new_index_name"
create_new_index

echon "Diffing old and new indexes from cluster:"
curl -s $cluster/$new_index_name?pretty > $new_index_information_json_file
diff_files $index_information_json_file $new_index_information_json_file

update_new_index_file_names

next_step "reindex documents from $index_name to $new_index_name"
source scripts/reindex.sh $cluster $index_name $new_index_name

next_step "move aliases from old index to new index"
check_continue

echon "Fetching aliases"
alert "currently disabled for safety"
source scripts/move_aliases.sh $cluster $index_name $new_index_name

recap
complete_message

exit
