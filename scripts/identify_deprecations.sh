#!/bin/bash

source util/_common.sh

if [ -n "$1" ]; then
    index_information_json_file=$1
else
    source ./util/_select_env.sh
    [ -z "$cluster" ] && log "! No cluster selected" && return
    source ./util/_select_index.sh $cluster
    [ -z "$index" ] && log "! No index selected" && return
    source ./util/_index_info.sh $cluster $index
    [ -z "$index_information_json_file" ] && log "! No index information available" && return
fi

source ./util/_scan_deprecations.sh $index_information_json_file
