#!/bin/bash

source util/_common.sh

if [[ -n "$1" ]]; then
    cluster="$1"
else
    source util/_select_env.sh
    [ -z "$cluster" ] && log "! No cluster selected" && return
fi
log "Selected cluster: $cluster"

if [[ -n "$2" ]]; then
    source_index="$2"
else
    source util/_select_index.sh $cluster
    [ -z "$index" ] && log "! No index selected" && return
    source_index=$index
    unset index
fi
log "Index A: $source_index"

if [[ -n "$3" ]]; then
    destination_index="$3"
else
    source util/_select_index.sh $cluster
    [ -z "$index" ] && log "! No index selected" && return
    destination_index=$index
    unset index
fi
log "Index B: $destination_index"

[ "$source_index" = "$destination_index" ] && log "! Can't select same index" && return

source ./util/_index_info.sh $cluster $source_index
[ -z "$index_information_json_file" ] && log "! No index information available" && return
source_json="$index_information_json_file"

source ./util/_index_info.sh $cluster $destination_index
[ -z "$index_information_json_file" ] && log "! No index information available" && return
destination_json="$index_information_json_file"

unset index_information_file

diff_files $source_json $destination_json
