#!/bin/bash

source util/_common.sh

source ./util/_select_env.sh
[ -z "$cluster" ] && echo "! No cluster selected" && return

source ./util/_select_index.sh $cluster
[ -z "$index" ] && echo "! No index selected" && return

count=$(source util/_document_count.sh $cluster $index)

echo -e "\nFor $index on $cluster:\n"
echo "There are $count documents"

sizes=$(curl -s "$cluster/$index/_stats/store")

echo -e "\nPrimary byte size: " && echo $sizes | jq --arg index_name "$index" '.indices[$index_name].primaries.store.size_in_bytes'
echo -e "\nTotal byte size: " && echo $sizes | jq --arg index_name "$index" '.indices[$index_name].total.store.size_in_bytes'
