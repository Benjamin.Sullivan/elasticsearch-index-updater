#!/bin/bash

source util/_common.sh

FIX_JSON_HEADER="
Select an index mappings-settings.json file to cure.
This script will update the JSON to fix some deprecations.

If not present in the output directory, go back to the
menu and run 'Get remote index info' to create this file.

"

local file=$(find . -type f -name '*mappings-settings.json' | fzf --header="$FIX_JSON_HEADER" | tr -d '[:space:]')
[ -z "$file" ] && log "! No file selected" && return
log "Selected file: $file"

cat $file >> original.json

source util/_fix_deprecations.sh $file

log "Changes in JSON:"
diff_files original.json $file
rm original.json

log "Checking JSON for remaining deprecations:"

source ./util/_scan_deprecations.sh $file
