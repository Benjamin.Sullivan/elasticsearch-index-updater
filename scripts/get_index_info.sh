#!/bin/bash

source util/_common.sh

source ./util/_select_env.sh
[ -z "$cluster" ] && log "! No cluster selected" && return
log "Selected cluster: $cluster"

source ./util/_select_index.sh $cluster
[ -z "$index" ] && log "! No index selected" && return
log "Selected index: $index"

source ./util/_index_info.sh $cluster $index
[ -z "$index_information_json_file" ] && log "! No index information available" && return


log "$index definition:"
cat $index_information_json_file | jq . | tee -a $LOG_FILE

log "File saved to: $index_information_json_file"

local index_stats_file="$OUTPUT_DIR/${cluster_name[$cluster_url]}-$index-stats.txt"
if [ -f "$index_stats_file" ]; then
    rm "$index_stats_file"
else
    touch "$index_stats_file"
fi

echo "$index" >> $index_stats_file

local count=$(source util/_document_count.sh $cluster $index)
echo "Num documents: $count" >> $index_stats_file

log "For $index on $cluster, there are $count documents"

local sizes=$(curl -s "$cluster/$index/_stats/store")
echo -e "\nPrimary byte size: " >> $index_stats_file
echo $sizes | jq --arg index_name "$index" '.indices[$index_name].primaries.store.size_in_bytes' >> $index_stats_file

echo -e "\nTotal byte size: "  >> $index_stats_file
echo $sizes | jq --arg index_name "$index" '.indices[$index_name].total.store.size_in_bytes' >> $index_stats_file

log "Primary byte size: " && echo $sizes | jq --arg index_name "$index" '.indices[$index_name].primaries.store.size_in_bytes' | tee -a $LOG_FILE
log "Total byte size: " && echo $sizes | jq --arg index_name "$index" '.indices[$index_name].total.store.size_in_bytes' | tee -a $LOG_FILE
