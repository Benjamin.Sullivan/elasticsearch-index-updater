#!/bin/bash

# $1 > cluster url
# $2 > source index name
# $3 > dest index name

source util/_common.sh

trap 'echo && return' SIGINT

if [[ -n "$1" ]]; then
    cluster="$1"
else
    source util/_select_env.sh
    [ -z "$cluster" ] && log "! No cluster selected" && return
fi
log "Selected cluster: $cluster"

if [[ -n "$2" ]]; then
    source_index="$2"
else
    source util/_select_index.sh $cluster
    [ -z "$index" ] && log "! No index selected" && return
    source_index=$index
    unset index
fi
log "Source index $source_index"

if [[ -n "$3" ]]; then
    destination_index="$3"
else
    source util/_select_index.sh $cluster
    [ -z "$index" ] && log "! No index selected" && return
    destination_index=$index
    unset index
fi
log "Destination index $destination_index"

[ "$source_index" = "$destination_index" ] && log "! Can't select same index" && return

source util/_index_info.sh $cluster $source_index
[ -z "$index_information_json_file" ] && log "! No index information available" && return

alias_actions=$(jq --arg source_index "$source_index" --arg destination_index "$destination_index" '{
actions: [
    .[$source_index].aliases | to_entries[] | .key as $alias |
    [{remove: {index: $source_index, alias: $alias}},
    {add: {index: $destination_index, alias: $alias}}]
] | flatten
}' "$index_information_json_file")

log "Alias actions:"
echo $alias_actions | jq '.' | tee -a $LOG_FILE

log "Moving aliases from $source_index to $destination_index on $cluster"
continue=$(check_continue)
local response=$OUTPUT_DIR/response.temp
status_code=$(curl -XPOST $cluster/_aliases -H 'Content-Type: application/json' -d "$alias_actions" -w "%{http_code}" -o $response -s)
log "Request status: $status_code"
log "Response: $(cat $response)"
rm $response
