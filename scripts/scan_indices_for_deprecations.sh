#!/bin/bash

source util/_common.sh

source util/_select_env.sh
[ -z "$cluster" ] && log "! No cluster selected" && return

index_names=$(curl -s "$cluster/_cat/indices" | awk '{print $3}')

results_dir="$OUTPUT_DIR/${cluster_name}-indices"
rm -rf $results_dir
mkdir -p $results_dir

results_file="$OUTPUT_DIR/${cluster_name}_deprecation_results.txt"
rm $results_file && touch $results_file

for index in $index_names; do
    log "Checking $index"
    curl -s "$cluster/$index?pretty" > "$results_dir/$index.json"
    result="$(util/_scan_deprecations.sh "$results_dir/$index.json")"
    if [ -n "$result" ]; then
        echo "--$index--" >> "$results_file"
        echo "$result" >> "$results_file"
        echo "---" >> "$results_file"
        echo "" >> "$results_file"
    fi
done
