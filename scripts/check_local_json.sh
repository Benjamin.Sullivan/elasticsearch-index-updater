#!/bin/bash


source util/_common.sh

LOCAL_JSON_HEADER="
Select an index mappings-settings.json file to check.

If not present in the output directory, go back to the
menu and run 'Get remote index info' to create this file.

"
file=$(find . -type f -name '*mappings-settings.json' | fzf --header="$LOCAL_JSON_HEADER" | tr -d '[:space:]')
[ -z "$file" ] && echo "! No file selected" && return

source util/_scan_deprecations.sh $file
