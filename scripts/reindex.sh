#!/bin/bash

# $1 > cluster url
# $2 > source index name
# $3 > dest index name

source util/_common.sh

trap "echo && return" SIGINT

if [[ -n "$1" ]]; then
    cluster="$1"
else
    source util/_select_env.sh
    [ -z "$cluster" ] && log "no cluster selected" && return
fi
log "Selected cluster: $cluster"

if [[ -n "$2" ]]; then
    source_index="$2"
else
    source util/_select_index.sh $cluster
    [ -z "$index" ] && log "! No index selected" && return
    source_index=$index
    unset index
fi
log "Source index: $source_index"

if [[ -n "$3" ]]; then
    destination_index="$3"
else
    source util/_select_index.sh $cluster
    [ -z "$index" ] && log "! No index selected" && return
    destination_index=$index
    unset index
fi
log "Destination index: $source_index"

[ "$source_index" = "$destination_index" ] && log "! Can't reindex to same index" && return

log "Reindexing from $source_index to $destination_index"

count=$(source util/_document_count.sh $cluster $source_index)
log "There are $count documents in $source_index"

wait_for_complete="false"
log "Wait for reindex to complete?"
case `select_opt "Yes" "No"` in
    0) wait_for_complete="true";;
esac

continue=$(check_continue)

spinner "$$" &
spinner_pid=$!

http_status_code=$(curl -s -o response.txt -w "%{http_code}" -XPOST "$cluster/_reindex?wait_for_completion=$wait_for_complete" -d '
{
    "source": {
        "index": "'"$source_index"'"
    },
    "dest": {
        "index": "'"$destination_index"'"
    }
}')
echo $http_status_code > status.txt

kill $spinner_pid
wait $spinner_pid 2>/dev/null

http_status_code=$(cat status.txt)

log "$(cat response.txt)"
rm response.txt status.txt

if [ "$http_status_code" -eq 200 ]; then
    log "Reindex successful!"
else
    log "Error! Status: $http_status_code"
    return
fi
