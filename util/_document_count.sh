#!/bin/bash

# $1 - cluster
# $2 - index name

count=$(curl -s $1/$2/_count | jq '.count')
echo $count
