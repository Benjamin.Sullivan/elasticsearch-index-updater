#!/bin/bash

# $1 > cluster url
# $2 > index name
# $3 > file path

local cluster_url=$1
local index_name=$2
local file_path=$3


source util/_common.sh

if [[ -n "$file_path" ]]; then
    index_information_json_file=$file_path
else
    index_information_json_file="$OUTPUT_DIR/${cluster_name[$cluster_url]}-$index_name.json"
    if [ -f "$index_information_json_file" ]; then
        rm "$index_information_json_file"
    else
        touch "$index_information_json_file"
    fi
fi

curl -s "$cluster_url/$index_name?pretty" > "$index_information_json_file"

mappings_settings_file="$OUTPUT_DIR/${cluster_name[$cluster_url]}-$index_name-mappings-settings.json"
if [ -f "$mappings_settings_file" ]; then
    rm "$mappings_settings_file"
else
    touch "$mappings_settings_file"
fi

extract_mappings_and_settings $index_information_json_file $mappings_settings_file
