#!/bin/bash

source util/_common.sh

# $1 path to JSON file with mappings and settings
index_information_json_file=$1

# echo "Checking for old version"
if jq -e '.. | .version?.created? | select(. != null) | startswith("1")' $index_information_json_file > /dev/null; then
    old_version=true
    log "!> This index was created before v2 and must be reindexed
https://angi.atlassian.net/wiki/spaces/DCM/pages/22544056546/DCMCAP-2+1.X+to+2.X+Index+Rebuild+Process
"
fi

# echo "Checking for _routing"
has_routing_path=$(jq '.. | objects | select(.mappings? != null or .settings? != null) | (.mappings?, .settings?.index?) | to_entries | .[] | select(.value._routing? and .value._routing.path != null) | {index: .key, routing: {type: .key, routing_info: .value._routing}}' $index_information_json_file)
if [ -n "$has_routing_path" ]; then
    log "!> index has _routing.path, needs to be fixed:
https://angi.atlassian.net/wiki/spaces/DCM/pages/22544056546/DCMCAP-2+1.X+to+2.X+Index+Rebuild+Process
"
fi

# echo "Checking for porterStem deprecation"
if jq -e '.. | .analysis?.analyzer[]?.filter? | select(. != null and . == ["porterStem"])' $index_information_json_file > /dev/null; then
    has_porter_stem=true
    log "!> porterStem deprecation in settings
"
fi

# echo "Checking for geo_point lat_lon deprecation"
if jq -e '.. | objects | select(.mappings? != null) | .mappings | to_entries | .[] | .value.properties? | to_entries | map(select(.value.type == "geo_point" and .value.lat_lon == true)) | length > 0 // false' $index_information_json_file > /dev/null; then
    has_lat_lon=true
    log "!> geo_point lat_lon in mappings
"
fi

# echo "Checking for geo_point geohash_prefix deprecation"
if jq -e '.. | objects | select(.mappings? != null) | .mappings | to_entries | .[] | .value.properties? | to_entries | map(select(.value.type == "geo_point" and .value.geohash_prefix == true)) | length > 0 // false' $index_information_json_file > /dev/null; then
    has_geohash_prefix=true
    log "!> geo_point geohash_prefix in mappings
"
fi

# echo "Checking for geo_point geohash_precision deprecation"
if jq -e '.. | objects | select(.mappings? != null) | .mappings | to_entries | .[] | .value.properties? | to_entries | map(select(.value.type == "geo_point" and .value.geohash_precision == true)) | length > 0 // false' $index_information_json_file > /dev/null; then
    has_geohash_precision=true
    log "!> geo_point geohash_precision in mappings
"
fi

# echo "Checking for geo_point geohash deprecation"
if jq -e '.. | objects | select(.mappings? != null) | .mappings | to_entries | .[] | .value.properties? | to_entries | map(select(.value.type == "geo_point" and .value.geohash == true)) | length > 0 // false' $index_information_json_file > /dev/null; then
    has_geohash=true
    log "!> geo_point geohash in mappings
"
fi

# echo "Checking for _timestamp deprecation"
has_timestamp=$(jq '.. | objects | .mappings? | to_entries? | map(select(.value._timestamp? != null)) | length > 0' $index_information_json_file)
if [ "$has_timestamp" = "true" ]; then
    log "!> index has _timestamp, needs to be fixed in reindex
https://www.elastic.co/guide/en/elasticsearch/reference/5.6/breaking_50_mapping_changes.html#_literal__ttl_literal_and_literal__timestamp_literal_cannot_be_created
"
fi

# echo "Checking for _ttl deprecation"
has_ttl=$(jq '.. | objects | .mappings? | to_entries? | map(select(.value._ttl? != null)) | length > 0' $index_information_json_file)
if [ "$has_ttl" = "true" ]; then
    log "!> index has _ttl, needs to be fixed
https://www.elastic.co/guide/en/elasticsearch/reference/5.6/breaking_50_mapping_changes.html#_literal__ttl_literal_and_literal__timestamp_literal_cannot_be_created
"
fi

# echo "Checking for index property deprecation"
# index_properties=$(jq '(.. | .properties? | select(type == "object" and .!=null) | to_entries[] | select((.value.index | tostring) != "true" and (.value.index | tostring) != "false" and .value.index != null and .value.type != "string")) // empty' $index_information_json_file)
# if [[ $index_properties ]]; then
#     has_bad_index_property=true
#     log "!> has mappings with index value that is not true or false
# - https://www.elastic.co/guide/en/elasticsearch/reference/5.6/breaking_50_mapping_changes.html#_literal_index_literal_property"
#     echo $index_properties | jq | tee -a $LOG_FILE
# fi
