#!/bin/bash

OUTPUT_DIR="output"
LOG_FILE=$OUTPUT_DIR/log.txt

DEV24_CLUSTER="http://main-elasticsearch.dev.angi.cloud"
QA24_CLUSTER="http://q3schpr003.homeadvisor.com:9200"
COMPARK_CLUSTER="http://pu020esh002.compark.homeadvisor.com:9200"
CORNELL_CLUSTER="http://p0schpr001.homeadvisor.com:9200"
DEV_GENERAL56_CLUSTER="https://angi-dev-es56-general-use.dev.angi.cloud"
DEV_PERFORMANCE56_CLUSTER="https://angi-dev-es56-performance.dev.angi.cloud"
DEV_STORAGE56_CLUSTER="https://angi-dev-es56-storage.dev.angi.cloud"
STG_GENERAL56_CLUSTER="https://angi-stage-es56-general-use.staging.angi.cloud"
STG_PERFORMANCE56_CLUSTER="https://angi-stage-es56-performance.staging.angi.cloud"
STG_STORAGE56_CLUSTER="https://angi-stage-es56-storage.staging.angi.cloud"
PROD_GENERAL56_CLUSTER="https://angi-prod-es56-general-use.proda.angi.cloud"
PROD_PERFORMANCE56_CLUSTER="https://angi-prod-es56-performance.proda.angi.cloud"
PROD_STORAGE56_CLUSTER="https://angi-prod-es56-storage.proda.angi.cloud"

declare -A cluster_name

cluster_name["$DEV24_CLUSTER"]="DEV24_CLUSTER"
cluster_name["$QA24_CLUSTER"]="QA24_CLUSTER"
cluster_name["$COMPARK_CLUSTER"]="COMPARK_CLUSTER"
cluster_name["$CORNELL_CLUSTER"]="CORNELL_CLUSTER"
cluster_name["$DEV_GENERAL56_CLUSTER"]="DEV_GENERAL56_CLUSTER"
cluster_name["$DEV_PERFORMANCE56_CLUSTER"]="DEV_PERFORMANCE56_CLUSTER"
cluster_name["$DEV_STORAGE56_CLUSTER"]="DEV_STORAGE56_CLUSTER"
cluster_name["$STG_GENERAL56_CLUSTER"]="STG_GENERAL56_CLUSTER"
cluster_name["$STG_PERFORMANCE56_CLUSTER"]="STG_PERFORMANCE56_CLUSTER"
cluster_name["$STG_STORAGE56_CLUSTER"]="STG_STORAGE56_CLUSTER"
cluster_name["$PROD_GENERAL56_CLUSTER"]="PROD_GENERAL56_CLUSTER"
cluster_name["$PROD_PERFORMANCE56_CLUSTER"]="PROD_PERFORMANCE56_CLUSTER"
cluster_name["$PROD_STORAGE56_CLUSTER"]="PROD_STORAGE56_CLUSTER"

echon() {
    echo -e "\n$@"
}

info() {
    echo " - $@"
}

alert() {
    echo " ! $@"
}

print_time() {
    echo "@ Time: $(date -u '+%H:%M:%S') UTC"
}

check_continue() {
    read -p "> Press enter to continue" -r
}

spinner() {
    local pid=$1
    local delay=0.1
    local spinstr='|/-\\'
    local start_time=$(date +%s)

    while [ "$(ps a | awk '{print $1}' | grep $pid)" ]; do
        local current_time=$(date +%s)
        local elapsed=$((current_time - start_time))

        # Format elapsed time as HH:MM:SS
        local hours=$((elapsed / 3600))
        local minutes=$(((elapsed / 60) % 60))
        local seconds=$((elapsed % 60))

        local temp=${spinstr#?}
        printf " [%c] %02d:%02d:%02d" "$spinstr" $hours $minutes $seconds
        local spinstr=$temp${spinstr%"$temp"}
        sleep $delay
        printf "\r\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b"
    done
    printf "    \r\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b"
}

extract_mappings_and_settings() {
    jq '{settings: .[].settings, mappings: .[].mappings}' $1 > $2
}

diff_files() {
    local indexA=$1
    local indexB=$2
    jq --sort-keys . $indexA > "$indexA.diff"
    jq --sort-keys . $indexB > "$indexB.diff"
    diff "$indexA.diff" "$indexB.diff" | tee -a $LOG_FILE
    rm "$indexA.diff"
    rm "$indexB.diff"
}

log() {
    echo -e "$(date '+%Y-%m-%d %H:%M:%S') - $1" | tee -a $LOG_FILE
}

function select_option {

    # little helpers for terminal print control and key input
    ESC=$( printf "\033")
    cursor_blink_on()  { printf "$ESC[?25h"; }
    cursor_blink_off() { printf "$ESC[?25l"; }
    cursor_to()        { printf "$ESC[$1;${2:-1}H"; }
    print_option()     { printf "   $1 "; }
    print_selected()   { printf "  $ESC[7m $1 $ESC[27m"; }
    get_cursor_row()   { IFS=';' read -sdR -p $'\E[6n' ROW COL; echo ${ROW#*[}; }
    key_input()        { read -s -n3 key 2>/dev/null >&2
                         if [[ $key = $ESC[A ]]; then echo up;    fi
                         if [[ $key = $ESC[B ]]; then echo down;  fi
                         if [[ $key = ""     ]]; then echo enter; fi; }

    # initially print empty new lines (scroll down if at bottom of screen)
    for opt; do printf "\n"; done

    # determine current screen position for overwriting the options
    local lastrow=`get_cursor_row`
    local startrow=$(($lastrow - $#))

    # ensure cursor and input echoing back on upon a ctrl+c during read -s
    trap "cursor_blink_on; stty echo; printf '\n'; exit" 2
    cursor_blink_off

    local selected=0
    while true; do
        # print options by overwriting the last lines
        local idx=0
        for opt; do
            cursor_to $(($startrow + $idx))
            if [ $idx -eq $selected ]; then
                print_selected "$opt"
            else
                print_option "$opt"
            fi
            ((idx++))
        done

        # user key control
        case `key_input` in
            enter) break;;
            up)    ((selected--));
                   if [ $selected -lt 0 ]; then selected=$(($# - 1)); fi;;
            down)  ((selected++));
                   if [ $selected -ge $# ]; then selected=0; fi;;
        esac
    done

    # cursor position back to normal
    cursor_to $lastrow
    printf "\n"
    cursor_blink_on

    return $selected
}

function select_opt {
    select_option "$@" 1>&2
    local result=$?
    echo $result
    return $result
}
