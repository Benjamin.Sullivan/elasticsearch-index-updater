#!/bin/bash

#$1 - cluster url

source util/_common.sh

log "Loading indices" && sleep 2
index="$(curl -s $1/_cat/indices | awk '{print $3}' | fzf --header "Select an index:" | tr -d '[:space:]')"
