#!/bin/bash

# set $cluster

DEV24_OPTION=" 1) ES2.4 Dev"
QA24_OPTION=" 2) ES2.4 QA3"
COMPARK_OPTION=" 3) ES2.4 Compark"
CORNELL_OPTION=" 4) ES2.4 Cornell"
DEV_GENERAL56_OPTION=" 5) ES5.6 Dev General Use"
DEV_PERFORMANCE56_OPTION=" 6) ES5.6 Dev Performance"
DEV_STORAGE56_OPTION=" 7) ES5.6 Dev Storage"
STG_GENERAL56_OPTION=" 8) ES5.6 Stg General Use"
STG_PERFORMANCE56_OPTION=" 9) ES5.6 Stg Performance"
STG_STORAGE56_OPTION="10) ES5.6 Stg Storage"
PROD_GENERAL56_OPTION="11) ES5.6 Prod General Use"
PROD_PERFORMANCE56_OPTION="12) ES5.6 Prod Performance"
PROD_STORAGE56_OPTION="13) ES5.6 Prod Storage"

options=(
    "$PROD_STORAGE56_OPTION"
    "$PROD_PERFORMANCE56_OPTION"
    "$PROD_GENERAL56_OPTION"
    "$STG_STORAGE56_OPTION"
    "$STG_PERFORMANCE56_OPTION"
    "$STG_GENERAL56_OPTION"
    "$DEV_STORAGE56_OPTION"
    "$DEV_PERFORMANCE56_OPTION"
    "$DEV_GENERAL56_OPTION"
    "$CORNELL_OPTION"
    "$COMPARK_OPTION"
    "$QA24_OPTION"
    "$DEV24_OPTION"
)
choice=$(printf '%s\n' "${options[@]}" | fzf --header "Select a cluster:" --cycle)

case $choice in
    "$DEV24_OPTION")
        cluster="$DEV24_CLUSTER"
        cluster_name="dev-2-4"
        ;;
    "$QA24_OPTION")
        cluster="$QA24_CLUSTER"
        cluster_name="qa-2-4"
        ;;
    "$COMPARK_OPTION")
        cluster="$COMPARK_CLUSTER"
        cluster_name="compark-2-4"
        ;;
    "$CORNELL_OPTION")
        cluster="$CORNELL_CLUSTER"
        cluster_name="cornell-2-4"
        ;;
    "$DEV_GENERAL56_OPTION")
        cluster="$DEV_GENERAL56_CLUSTER"
        cluster_name="dev-general-5-6"
        ;;
    "$DEV_PERFORMANCE56_OPTION")
        cluster="$DEV_PERFORMANCE56_CLUSTER"
        cluster_name="dev-performance-5-6"
        ;;
    "$DEV_STORAGE56_OPTION")
        cluster="$DEV_STORAGE56_CLUSTER"
        cluster_name="dev-storage-5-6"
        ;;
    "$STG_GENERAL56_OPTION")
        cluster="$STG_GENERAL56_CLUSTER"
        cluster_name="stg-general-5-6"
        ;;
    "$STG_PERFORMANCE56_OPTION")
        cluster="$STG_PERFORMANCE56_CLUSTER"
        cluster_name="stg-performance-5-6"
        ;;
    "$STG_STORAGE56_OPTION")
        cluster="$STG_STORAGE56_CLUSTER"
        cluster_name="stg-storage-5-6"
        ;;
    "$PROD_GENERAL56_OPTION")
        cluster="$PROD_GENERAL56_CLUSTER"
        cluster_name="prod-general-5-6"
        ;;
    "$PROD_PERFORMANCE56_OPTION")
        cluster="$PROD_PERFORMANCE56_CLUSTER"
        cluster_name="prod-performance-5-6"
        ;;
    "$PROD_STORAGE56_OPTION")
        cluster="$PROD_STORAGE56_CLUSTER"
        cluster_name="prod-storage-5-6"
        ;;
    *)
        echo "Invalid option: $1"
        ;;
esac
