#!/bin/bash

json_file=$1

jq '(.settings.index.analysis.analyzer[] | select(.filter?) | .filter) |= map(if . == "porterStem" then "porter_stem" else . end)' $json_file > temp.json && mv temp.json $json_file
jq 'walk(if type == "object" and .type == "geo_point" and has("lat_lon") then del(.lat_lon) else . end)' $json_file > temp.json && mv temp.json $json_file
jq 'walk(if type == "object" and .type == "geo_point" and has("geohash_prefix") then del(.geohash_prefix) else . end)' $json_file > temp.json && mv temp.json $json_file
jq 'walk(if type == "object" and .type == "geo_point" and has("geohash_precision") then del(.geohash_precision) else . end)' $json_file > temp.json && mv temp.json $json_file
jq 'walk(if type == "object" and .type == "geo_point" and has("geohash") then del(.geohash) else . end)' $json_file > temp.json && mv temp.json $json_file

